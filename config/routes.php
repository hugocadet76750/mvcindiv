<?php

$routes = array(
    array('home','default','index'),
    // ADMIN
    array('admin','admin','index'),
    // SUBS
    array('subs','subs','index'),
    array('add-sub','subs','add'),
    array('listing-subs','subs','listing'),
    array('single-sub','subs','single',array('id')),
    array('modify-sub','subs','modify',array('id')),
    array('delete-sub','subs','delete',array('id')),
    // PRODUCTS
    array('products','products','index'),
    array('add-product','products','add'),
    array('listing-products','products','listing'),
    array('single-product','products','single',array('id')),
    array('modify-product','products','modify',array('id')),
    array('delete-product','products','delete',array('id')),
    // BORROW
    array('listing-borrows','borrows','listing'),
    array('historic-borrows','borrows','historic'),
    array('single-borrows','borrows','single',array('id')),
    array('enddate-borrows','borrows','enddate',array('id')),
    // STATS
    array('stats','stats','stats'),
);









