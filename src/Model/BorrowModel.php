<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class BorrowModel extends AbstractModel
{
    protected static $table = 'borrows';
    protected int $id;
    protected int $id_sub;
    protected int $id_product;
    protected $date_start;
    protected $date_end;

    public static function allBorrowsBy($type,$order)
    {
        return App::getDatabase()->query("SELECT b.id, s.lname, s.fname, p.title, b.date_start, b.date_end FROM ".self::getTable(). " AS b LEFT JOIN subs AS s ON s.id = b.id_subs LEFT JOIN products AS p ON p.id = b.id_products ORDER BY $type $order" ,get_called_class());
    }

    public static function allBorrowsByWhereId($type,$order,$id)
    {
        return App::getDatabase()->query("SELECT b.id, s.lname, s.fname, p.title, b.date_start, b.date_end FROM ".self::getTable(). " AS b LEFT JOIN subs AS s ON s.id = b.id_subs LEFT JOIN products AS p ON p.id = b.id_products WHERE b.id_subs = $id ORDER BY $type $order" ,get_called_class());
    }

    public static function allBorrowsByDateEndNotNull($type,$order)
    {
        return App::getDatabase()->query("SELECT b.id, s.lname, s.fname, p.title, b.date_start, b.date_end FROM ".self::getTable(). " AS b LEFT JOIN subs AS s ON s.id = b.id_subs LEFT JOIN products AS p ON p.id = b.id_products  WHERE b.date_end IS NULL ORDER BY $type $order"  ,get_called_class());
    }

    public static function allBorrowsByDateEndNull($type,$order)
    {
        return App::getDatabase()->query("SELECT b.id, s.lname, s.fname, p.title, b.date_start, b.date_end FROM ".self::getTable(). " AS b LEFT JOIN subs AS s ON s.id = b.id_subs LEFT JOIN products AS p ON p.id = b.id_products  WHERE b.date_end IS NOT NULL ORDER BY $type $order"  ,get_called_class());
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_subs,id_products,date_start) VALUES (?,?,NOW())",  array($post['subs'],($post['products'])));
    }

    public static function update($id)
    {
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET date_end = NOW() WHERE id = ?", array($id));
    }

    public static function delete($id,$columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function countDateEndNull(){
        return App::getDatabase()->aggregation("SELECT COUNT(id), b.date_end FROM " . self::getTable() . " AS b WHERE b.date_end IS NULL");
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdSub(): int
    {
        return $this->id_sub;
    }

    /**
     * @return int
     */
    public function getIdProduct(): int
    {
        return $this->id_product;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }



}