<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SubsModel extends AbstractModel
{
    protected static $table = 'subs';
    protected int $id;
    protected string $fname;
    protected string $lname;
    protected string $email;
    protected int $age;
    protected string $created_at;

    public static function allSubsBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (lname,fname,email,age,created_at) VALUES (?,?,?,?,NOW())",array($post['lname'],$post['fname'],$post['email'],$post['age']));
    }

    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET lname = ?, fname = ?, email = ?, age = ? WHERE id = ?", array($post['lname'],$post['fname'],$post['email'],$post['age'], $id));
    }

    public static function delete($id,$columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function selectEmail($post)
    {
        return App::getDatabase()->prepare("SELECT * FROM ".self::getTable(). " WHERE email = ?" ,array($post['email']), get_called_class(), true);
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFname(): string
    {
        return $this->fname;
    }

    /**
     * @return string
     */
    public function getLname(): string
    {
        return $this->lname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

}