<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductsModel extends AbstractModel
{
    protected static $table = 'products';
    protected int $id;
    protected string $title;
    protected string $reference;
    protected string $description;

    public static function allProductsBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (title,reference,description) VALUES (?,?,?)",array($post['title'],$post['reference'],$post['description']));
    }

    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET title = ?, reference = ?, description = ? WHERE id = ?", array($post['title'],$post['reference'],$post['description'], $id));
    }

    public static function delete($id,$columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

}