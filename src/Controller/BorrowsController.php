<?php

namespace App\Controller;

use App\Model\BorrowModel;
use App\Model\ProductsModel;
use App\Model\SubsModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class BorrowsController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function listing()
    {
        $textButton = 'Schedule borrow';
        $borrows = BorrowModel::allBorrowsByDateEndNotNull('id','DESC');
        $subs = SubsModel::allSubsBy('id','DESC');
        $products = ProductsModel::allProductsBy('id','DESC');
        $errors = [];
        if (!empty($_POST['submitted'])) {
            // Faille XSS
            $post = $this->cleanXss($_POST);
            // Validation
            $errors = $this->validate($this->v,$post);
            $validation = new Validation();
            $this->validate($validation, $post);
            if ($validation->IsValid($errors)) {
                BorrowModel::insert($post);
                $this->redirect('listing-borrows');
            }
        }
        $form = New Form($errors);
        $title = 'Borrow Listing';
        $borrowsTitle = 'Borrows';
        $this->render('app.admin.listingborrows',array(
            'borrows' => $borrows,
            'subs' => $subs,
            'products' => $products,
            'title' => $title,
            'form' => $form,
            'textButton' => $textButton,
            'borrowsTitle' => $borrowsTitle,
        ), 'admin');
    }

    public function historic()
    {
        $textButton = 'Schedule borrow';
        $borrows = BorrowModel::allBorrowsByDateEndNull('id','DESC');
        $title = 'Borrow Historic';
        $borrowsTitle = 'Borrows';
        $this->render('app.admin.historicborrows',array(
            'borrows' => $borrows,
            'title' => $title,
            'textButton' => $textButton,
            'borrowsTitle' => $borrowsTitle,
        ), 'admin');
    }

    public function enddate($id)
    {
        if (is_numeric($id)) {
            $this->getBorrowsByIdOr404($id);
            BorrowModel::update($id);
            $this->redirect('listing-borrows');
        }
    }

    private function validate($v,$post)
    {
        $errors = [];
        $verifSub = SubsModel::findById($post['subs']);
        $verifProd = ProductsModel::findById($post['products']);
        if(empty($verifSub)) {
            $errors['subs'] = 'Error';
        }
        if(empty($verifProd)) {
            $errors['products'] = 'Error';
        }
        return $errors;
    }

    private function getBorrowsByIdOr404($id){
        $borrows = BorrowModel::findById($id);
        if(empty($borrows)) {
            $this->Abort404();
        }
        return $borrows;
    }
}