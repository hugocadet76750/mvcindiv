<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class AdminController extends AbstractController
{
    public function index()
    {
        $message = 'Adminsitration';
        $this->render('app.admin.index',array(
            'message' => $message,
        ), 'admin');
    }
}
