<?php

namespace App\Controller;

use App\Model\SubsModel;
use App\Model\BorrowModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class SubsController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $message = 'Subs';
        $this->render('app.admin.subs',array(
            'message' => $message,
        ), 'admin');
    }

    public function listing()
    {
        $subs = SubsModel::allSubsBy('id','DESC');

        $title = 'Listing';
        $subsTitle = 'Subs';
        $this->render('app.admin.listingsubs',array(
            'subs' => $subs,
            'title' => $title,
            'subsTitle' => $subsTitle,
        ), 'admin');
    }

    public function add()
    {
        $subsTitle = 'Adding sub';
        // FORM ADD //
        $errors = [];
        if (!empty($_POST['submitted'])) {
            // Faille XSS
            $post = $this->cleanXss($_POST);
            // Validation
            $errors = $this->validate($this->v,$post);
            $existing = SubsModel::selectEmail($post);
            if (empty($errors['email'])) {
                if (!empty($existing)) {
                    $errors['email'] = 'This mail already exists';
                }
                $validation = new Validation();
                $this->validate($validation, $post);
                if ($validation->IsValid($errors)) {
                    SubsModel::insert($post);
                    $this->redirect('listing-subs');
                }
            }
        }
        $form = New Form($errors);
        $this->render('app.admin.addsub',array(
            'form' => $form,
            'subsTitle' => $subsTitle,
        ), 'admin');
    }

    public function modify($id)
    {
        $subsTitle = 'Modifying sub';
        $errors = array();
        $uniquesubs = $this->getSubByIdOr404($id);
        if (!empty($uniquesubs)) {
            if (!empty($_POST['submitted'])) {
                // Faille XSS
                $post = $this->cleanXss($_POST);
                // Validation
                $validation = new Validation();
                $this->validate($validation, $post);
                if ($validation->IsValid($errors)) {
                    SubsModel::update($id, $post);
                    $this->redirect('listing-subs');
                }
            }
            $form = new Form($errors);
            $this->render('app.admin.modifysub', array(
                'uniquesubs' => $uniquesubs,
                'form' => $form,
                'subsTitle' => $subsTitle,
            ), 'admin');
        }
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $this->getSubByIdOr404($id);
            SubsModel::delete($id);
            $this->redirect('listing-subs');
        }
    }

    public function single($id)
    {
        $borrows = BorrowModel::allBorrowsByWhereId('id','DESC',$id);
        $uniquesubs = $this->getSubByIdOr404($id);
            $this->render('app.admin.singlesub', array(
                'uniquesubs' => $uniquesubs,
                'borrows' => $borrows,
            ), 'admin');
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['lname'] = $v->textValid($post['lname'], 'last name',2, 100);
        $errors['fname'] = $v->textValid($post['fname'], 'first name',2, 100);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 200);
        $errors['age'] = $v->textValid($post['age'], 'email',1, 3);
        if (!is_numeric($post['age'])) {
            $errors['age'] = 'Please insert a numeric number';
        }
        return $errors;
    }

    private function getSubByIdOr404($id){
        $subs = SubsModel::findById($id);
        if(empty($subs)) {
            $this->Abort404();
        }
        return $subs;
    }
}
