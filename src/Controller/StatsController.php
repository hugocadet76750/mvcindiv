<?php

namespace App\Controller;

use App\Model\BorrowModel;
use App\Model\ProductsModel;
use App\Model\SubsModel;
use Core\Kernel\AbstractController;

class StatsController extends AbstractController
{

    public function stats()
    {
        $subs = SubsModel::count();
        $products = ProductsModel::count();
        $borrows = BorrowModel::count();
        $borrowsnull = BorrowModel::countDateEndNull();
        $title = 'Stats';
        $this->render('app.admin.stats',array(
            'subs' => $subs,
            'products' => $products,
            'borrows' => $borrows,
            'borrowsnull' => $borrowsnull,
            'title' => $title,
        ), 'admin');
    }
}