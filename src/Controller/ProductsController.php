<?php

namespace App\Controller;

use App\Model\ProductsModel;
use App\Service\Form;
use App\Service\Validation;
use JasonGrimes\Paginator;
use Core\Kernel\AbstractController;

/**
 *
 */
class ProductsController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $message = 'Products';
        $this->render('app.admin.products',array(
            'message' => $message,
        ), 'admin');
    }

    public function listing()
    {
        $products = ProductsModel::allProductsBy('id','DESC');
        $currentPage = 1;
        $totalItems = ProductsModel::count();
        $itemsPerPage = 2;
        $urlPattern = '/listing-products?page=(:num)';
        if (!empty($_GET['page'])) {
            $currentPage = $_GET['page'];
        }
        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

        $title = 'Listing';
        $productsTitle = 'Products';
        $this->render('app.admin.listingproducts',array(
            'products' => $products,
            'title' => $title,
            'productsTitle' => $productsTitle,
            'paginator' => $paginator,
            'totalItems' => $totalItems,
            'itemsPerPage' => $itemsPerPage,
            'currentPage' => $currentPage,
            'urlPattern' => $urlPattern,
        ), 'admin');
    }

    public function add()
    {
        $productsTitle = 'Adding product';
        // FORM ADD //
        $errors = [];
        if (!empty($_POST['submitted'])) {
            // Faille XSS
            $post = $this->cleanXss($_POST);
            // Validation
            $errors = $this->validate($this->v,$post);
            $validation = new Validation();
            $this->validate($validation, $post);
            if ($validation->IsValid($errors)) {
                ProductsModel::insert($post);
                $this->redirect('listing-products');
            }
        }
        $form = New Form($errors);
        $this->render('app.admin.addproduct',array(
            'form' => $form,
            'productsTitle' => $productsTitle,
        ), 'admin');
    }

    public function modify($id)
    {
        $productsTitle = 'Modifying product';
        $errors = array();
        $uniqueproducts = $this->getProductsByIdOr404($id);
        if (!empty($uniqueproducts)) {
            if (!empty($_POST['submitted'])) {
                // Faille XSS
                $post = $this->cleanXss($_POST);
                // Validation
                $validation = new Validation();
                $this->validate($validation, $post);
                if ($validation->IsValid($errors)) {
                    ProductsModel::update($id, $post);
                    $this->redirect('listing-products');
                }
            }
            $form = new Form($errors);
            $this->render('app.admin.modifyproduct', array(
                'uniqueproducts' => $uniqueproducts,
                'form' => $form,
                'productsTitle' => $productsTitle,
            ), 'admin');
        }
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $this->getProductsByIdOr404($id);
            ProductsModel::delete($id);
            $this->redirect('listing-products');
        }
    }

    public function single($id)
    {
        $uniqueproducts = $this->getProductsByIdOr404($id);
            $this->render('app.admin.singleproduct', array(
                'uniqueproducts' => $uniqueproducts,
            ), 'admin');
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 100);
        $errors['reference'] = $v->textValid($post['reference'], 'reference',2, 100);
        $errors['description'] = $v->textValid($post['description'], 'description',5, 200);
        return $errors;
    }

    private function getProductsByIdOr404($id){
        $product = ProductsModel::findById($id);
        if(empty($product)) {
            $this->Abort404();
        }
        return $product;
    }
}
