<h1>
    <?= $uniquesubs->lname.' '.$uniquesubs->fname ?>
</h1>

<div class="singlesub wrap">
    <div class="uniquesubs">
        <p>ID :<span><?= $uniquesubs->id ?></span></p>
        <p>LAST NAME : <span><?= $uniquesubs->lname ?></span></p>
        <p>FIRST NAME : <span><?= $uniquesubs->fname ?></span></p>
        <p>MAIL ADDRESS : <span><?= $uniquesubs->email ?></span></p>
        <p>AGE : <span><?= $uniquesubs->age ?></span></p>
    </div>

    <div class="return">
        <a href="<?= $view->path('modify-sub/' . $uniquesubs->id); ?>">MODIFY</a>
        <a href="<?= $view->path('delete-sub/' . $uniquesubs->id); ?>">DELETE</a>
        <a href="<?= $view->path('listing-subs'); ?>">RETURN</a>
    </div>

    <div class="uniquesubs borrowsub">
        <?php
            foreach ($borrows as $uniqueborrows) {
                echo '<p class="unique_listing"><span>Sub :</span>' . $uniqueborrows->fname . '<span>Product :</span>' . $uniqueborrows->title . '<span>Date start :</span>' . date('d/m/Y h:i a', strtotime($uniqueborrows->getDateStart())).'<span>Date end :</span>' . date('d/m/Y h:i a', strtotime($uniqueborrows->getDateEnd()));
                echo '</p>';
            }
        ?>
    </div>


</div>