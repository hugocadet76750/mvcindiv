<h1>
    <?= $uniqueproducts->title ?>
</h1>

<div class="singlesub wrap">
    <div class="uniquesubs">
        <p>ID :<span><?= $uniqueproducts->id ?></span></p>
        <p>TITLE : <span><?= $uniqueproducts->title ?></span></p>
        <p>REFERENCE : <span><?= $uniqueproducts->reference ?></span></p>
        <p>DESCRIPTION : <span><?= $uniqueproducts->description ?></span></p>
    </div>

    <div class="return">
        <a href="<?= $view->path('modify-product/' . $uniqueproducts->id); ?>">MODIFY</a>
        <a href="<?= $view->path('delete-product/' . $uniqueproducts->id); ?>">DELETE</a>
        <a href="<?= $view->path('listing-products'); ?>">RETURN</a>
    </div>
</div>