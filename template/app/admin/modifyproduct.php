<h1>
    <?= $productsTitle; ?>
</h1>
<div class="wrap">
    <form action="" method="post">
        <?= $form->label('Title'); ?>
        <?= $form->input('title','text', $uniqueproducts->title); ?>
        <?= $form->error('title'); ?>
        <?= '<br>' ?>
        <?= $form->label('Reference'); ?>
        <?= $form->input('reference','text', $uniqueproducts->reference); ?>
        <?= $form->error('reference'); ?>
        <?= '<br>' ?>
        <?= $form->label('Description'); ?>
        <?= $form->input('description','text', $uniqueproducts->description); ?>
        <?= $form->error('description'); ?>
        <?= '<br>' ?>
        <?= $form->submit('submitted','MODIFY') ?>
    </form>
</div>