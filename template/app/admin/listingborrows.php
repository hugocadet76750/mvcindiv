<h1>
    <?= $title; ?>
</h1>

<div class="addborrow">
    <?php include('form.php'); ?>
</div>

<div class="listings wrap">
    <div class="borrows">
        <?php
            foreach ($borrows as $uniqueborrows) {
                echo '<p class="unique_listing"><span>Sub :</span>' . $uniqueborrows->fname . '<span>Product :</span>' . $uniqueborrows->title . '<span>Date start :</span>' . date('d/m/Y h:i a', strtotime($uniqueborrows->getDateStart()));
                echo '<a class="end" href="'.$view->path('enddate-borrows/' . $uniqueborrows->id).'">END</a>';
                echo '</p>';
            }
        ?>
    </div>
</div>