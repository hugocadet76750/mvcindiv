<h1>
    <?= $subsTitle; ?>
</h1>
<div class="wrap">
    <form action="" method="post">
        <?= $form->label('Last name'); ?>
        <?= $form->input('lname','text'); ?>
        <?= $form->error('lname'); ?>
        <?= '<br>' ?>
        <?= $form->label('First name'); ?>
        <?= $form->input('fname','text'); ?>
        <?= $form->error('fname'); ?>
        <?= '<br>' ?>
        <?= $form->label('E-Mail'); ?>
        <?= $form->input('email','email'); ?>
        <?= $form->error('email'); ?>
        <?= '<br>' ?>
        <?= $form->label('Age'); ?>
        <?= $form->input('age','text'); ?>
        <?= $form->error('age'); ?>
        <?= '<br>' ?>
        <?= $form->submit('submitted','ADD') ?>
    </form>
</div>