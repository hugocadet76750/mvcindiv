<h1>
    <?= $productsTitle; ?>
</h1>
<div class="wrap">
    <form action="" method="post">
        <?= $form->label('Title'); ?>
        <?= $form->input('title','text'); ?>
        <?= $form->error('title'); ?>
        <?= '<br>' ?>
        <?= $form->label('Reference'); ?>
        <?= $form->input('reference','text'); ?>
        <?= $form->error('reference'); ?>
        <?= '<br>' ?>
        <?= $form->label('Description'); ?>
        <?= $form->input('description','text'); ?>
        <?= $form->error('description'); ?>
        <?= '<br>' ?>
        <?= $form->submit('submitted','ADD') ?>
    </form>
</div>