<form action="" method="post" novalidate class="select">
    <?php echo $form->label('Sub :'); ?>
    <?php echo $form->select('subs', $subs, 'fname', $borrows->subs_id ?? ''); ?>
    <?php echo $form->error('sub'); ?>

    <?php echo $form->label('Product :'); ?>
    <?php echo $form->select('products', $products, 'title', $borrows->products_id ?? ''); ?>
    <?php echo $form->error('product'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>
