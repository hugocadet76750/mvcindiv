<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
      <title>Administration</title>
    <?php echo $view->add_webpack_style('admin'); ?>
  </head>
  <body>
  <?php // $view->dump($view->getFlash()) ?>
  <div class="sidebar">
      <div class="img">
          <a href="">
              <img src="<?php echo $view->asset('img/logo2.png'); ?>" alt="">
          </a>
      </div>
      <nav>
          <ul>
              <li><a href="<?= $view->path('subs'); ?>">SUBS</a></li>
              <li><a href="<?= $view->path('products'); ?>">PRODUCTS</a></li>
              <li><a href="<?= $view->path('listing-borrows'); ?>">BORROWS</a></li>
              <li><a href="<?= $view->path('historic-borrows'); ?>">BORROWS HISTORIC</a></li>
              <li><a href="<?= $view->path('listing-subs'); ?>">BORROWS HISTORIC / SUBS</a></li>
              <li><a href="<?= $view->path('stats'); ?>">STATS</a></li>
          </ul>
      </nav>
  </div>

  <div class="wholesite">
      <header id="masthead">
          <nav>
              <ul>
                  <li><a href="<?= $view->path(''); ?>">HOME</a></li>
                  <li><a href="<?= $view->path('admin'); ?>">ADMIN</a></li>
              </ul>
          </nav>
      </header>

      <div class="container">
          <?= $content; ?>
      </div>

      <footer id="colophon">
          <div class="wrap">
              <p>MVC 6 - Framework Pédagogique.</p>
          </div>
      </footer>
  </div>
  <?php echo $view->add_webpack_script('admin'); ?>
  </body>
</html>
